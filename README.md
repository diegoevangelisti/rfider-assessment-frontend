## Rfider assessment - Front End

Done by: Diego Evangelisti

## Goal

The goal of this assessment is not to change the look and feel. We will be evaluating component patterns for reusability and performance.

- Create a single component to be used to render each list in the page. This component should include the pagination component inside.
- Decide which component pattern to use - High order components vs components using slots.

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
